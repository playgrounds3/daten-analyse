from os import listdir
from os.path import isfile, join, isdir, abspath
from typing import List, Tuple
import pandas as pd

COL_DATETIME = 'Zeitpunkt'
TIMEZONE = 'Europe/Zurich'


def read_data(directory: str, filename_match:str) -> pd.DataFrame:
    files = _fetch_filenames(directory=directory, filename_match=filename_match)

    # read data
    print("Reading Data...")
    dataframes = [_read_file(fn) for fn in files]

    # concatenate
    print("Concatenating...")
    df = pd.concat(dataframes)

    # Parse Date
    print("Parsing Datetime...")
    df[COL_DATETIME] = pd.to_datetime(df[COL_DATETIME], utc=True).dt.tz_convert(TIMEZONE)

    # Order by Date
    df = df.sort_values(by=[COL_DATETIME])

    return df


def _read_file(filepath: str) -> pd.DataFrame:
    # Read File
    print("Reading data from file:", filepath)
    data = pd.read_csv(filepath)

    return data


def _fetch_filenames(directory: str, filename_match:str) -> List[str]:
    """
    :param directory: path of a directory
    :return: the paths to the csv files in the directory and files in all subdirectory recursively
    """
    print('Fetching files from:', directory)
    dir_content = [abspath(join(directory, f)) for f in listdir(directory)]
    csv_files = [f for f in dir_content if isfile(f) if f.endswith(".csv") and filename_match in f]
    subdirectories = [d for d in dir_content if isdir(d)]
    for d in subdirectories:
        csv_files.extend(_fetch_filenames(directory=d, filename_match=filename_match))

    return csv_files
