# Analyse WP-Logs
Die Notebooks (ipynb Dateien) enthalten interessante Plots.

# Daten
Die Daten sind im Ordner "data" untergebracht.


# Binder 
Klicken um die Analyse zu starten:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/playgrounds3%2Fdaten-analyse/main)