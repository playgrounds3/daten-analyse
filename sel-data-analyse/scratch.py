
import data_reader
import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model
import statsmodels.api as sm

COL_DATETIME = 'Zeitpunkt'
WOHNUNG_COL = "Wohnung"
COL_ENERGIE="Energieverbrauch Elektro - Total [kWh]"
COL_WW="Warmwasserverbrauch [m3]"
COL_HEIZ="Energieverbrauch Heizung [kWh]"

COL_FACTOR = 'factor'

def group_by_date(df):
    return df.groupby(df[COL_DATETIME].dt.normalize()).sum()


if __name__ == '__main__':
    energie_df = data_reader.read_data(directory="../data/sel-data", filename_match="haus")
    alg_energie = energie_df[energie_df[WOHNUNG_COL] == "ALLGEMEIN"]
    alg_energie = alg_energie.groupby(by=[COL_DATETIME], axis=0, as_index=False).sum()
    energie_data = alg_energie[[COL_DATETIME, COL_ENERGIE]]
    energie_data = group_by_date(energie_data)

    ww_df = data_reader.read_data(directory="../data/sel-data", filename_match="warm")
    ww_data = ww_df.groupby(by=[COL_DATETIME], axis=0, as_index=False).sum()
    ww_data = group_by_date(ww_data)

    heiz_df = data_reader.read_data(directory="../data/sel-data", filename_match="heiz")
    heiz_data = heiz_df.groupby(by=[COL_DATETIME], axis=0, as_index=False).sum()
    heiz_data = group_by_date(heiz_data)

    data = energie_data.merge(ww_data, on=COL_DATETIME, how="outer")
    data = data.merge(heiz_data, on=COL_DATETIME, how="outer")
    data = data.fillna(0)


    print(data.head())
    print(data.columns)

    X = data[[COL_WW, COL_HEIZ]]
    Y = data[COL_ENERGIE]

    # with sklearn
    regr = linear_model.LinearRegression()
    regr.fit(X, Y)

    print('Constant: \n', regr.intercept_)
    print('Coefficients: \n', regr.coef_)
    constant = regr.intercept_
    coeff = regr.coef_
    print(f"{constant}+{coeff[0]}*ww + {coeff[1]}*heiz = stromverbrauch")

    # with statsmodels
    X = sm.add_constant(X)  # adding a constant

    model = sm.OLS(Y, X).fit()
    predictions = model.predict(X)

    print_model = model.summary()
    print(print_model)

    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure(figsize=(12, 9))
    ax = Axes3D(fig)

    ax.scatter(data[COL_WW], data[COL_HEIZ], data[COL_ENERGIE])

    plt.show()