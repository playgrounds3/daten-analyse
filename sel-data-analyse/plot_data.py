import pandas as pd
import numpy as np
import plotly.graph_objects as go
import plotly.express as px
import plotly.offline as py

from constants import *
from load_data import *
from manipulate_data import *


# --- Energie Verbrauch --- #
def plot_energieverbrauch(df: pd.DataFrame, title='Energieverbrauch pro Stunde', **kwargs):
    return _barplot(df, x=COL_DATETIME, y=COL_ENERGIEVERBRAUCH, color=COL_WOHNUNG, title=title, **kwargs)

def plot_energieverbrauch_by_hour(df: pd.DataFrame, title='Energieverbrauch pro Stunde', **kwargs):
    return _barplot_by_hour(df, x_col=COL_DATE_HOUR, verbrauch_col=COL_ENERGIEVERBRAUCH, title=title, **kwargs)

def plot_energieverbrauch_by_day(df: pd.DataFrame, title='Energieverbrauch pro Tag', **kwargs):
    return _barplot_by_day(df, x_col=COL_DATE, verbrauch_col=COL_ENERGIEVERBRAUCH, title=title, **kwargs)

def boxplot_energieverbrauch(df: pd.DataFrame, title='Energieverbrauch', **kwargs):
    return _boxplot(df, x=COL_DATE, y=COL_ENERGIEVERBRAUCH, title=title, **kwargs)

# --- Warmwasser Verbrauch --- #
def plot_warmwasserverbrauch(df: pd.DataFrame, title='Energieverbrauch pro Stunde', **kwargs):
    return _barplot(df, x=COL_DATETIME, y=COL_WARMWASSERVERBRAUCH, color=COL_WOHNUNG, title=title, **kwargs)

def plot_warmwasserverbrauch_by_hour(df: pd.DataFrame, title='Warmwasserverbrauch pro Stunde', **kwargs):
    return _barplot_by_hour(df, x_col=COL_DATE_HOUR, verbrauch_col=COL_WARMWASSERVERBRAUCH, title=title, **kwargs)

def plot_warmwasserverbrauch_by_day(df: pd.DataFrame, title='Warmwasserverbrauch pro Tag', **kwargs):
    return _barplot_by_day(df, x_col=COL_DATE, verbrauch_col=COL_WARMWASSERVERBRAUCH, title=title, **kwargs)

def boxplot_warmwasserverbrauch(df: pd.DataFrame, title='Warmwasserverbrauch', **kwargs):
    return _boxplot(df, x=COL_DATE, y=COL_WARMWASSERVERBRAUCH, title=title, **kwargs)

# --- Heizenergie Verbrauch --- #
def plot_heizenergieverbrauch(df: pd.DataFrame, title='Energieverbrauch pro Stunde', **kwargs):
    return _barplot(df, x=COL_DATETIME, y=COL_HEIZENERGIEVERBRAUCH, color=COL_WOHNUNG, title=title, **kwargs)

def plot_heizenergieverbrauch_by_hour(df: pd.DataFrame, title='Heiz Energieverbrauch pro Stunde', **kwargs):
    return _barplot_by_hour(df, x_col=COL_DATE_HOUR, verbrauch_col=COL_HEIZENERGIEVERBRAUCH, title=title, **kwargs)

def plot_heizenergieverbrauch_by_day(df: pd.DataFrame, title='Heiz Energieverbrauch pro Tag', **kwargs):
    return _barplot_by_day(df, x_col=COL_DATE, verbrauch_col=COL_HEIZENERGIEVERBRAUCH, title=title, **kwargs)

def boxplot_heizenergieverbrauch(df: pd.DataFrame, title='Heiz Energieverbrauch', **kwargs):
    return _boxplot(df, x=COL_DATE, y=COL_HEIZENERGIEVERBRAUCH, title=title, **kwargs)


# --- Helpers --- #

def _barplot_by_hour(df: pd.DataFrame, x_col:str, verbrauch_col:str, **kwargs):
    df_by_hour = group_by_hour(df, verbrauch_col=verbrauch_col)
    fig = _barplot(df_by_hour, x=x_col, y=verbrauch_col, color=COL_WOHNUNG, **kwargs)
    return fig

def _barplot_by_day(df: pd.DataFrame, x_col:str, verbrauch_col:str, **kwargs):
    df_by_day = group_by_day(df, verbrauch_col=verbrauch_col)
    fig = _barplot(df_by_day, x=x_col, y=verbrauch_col, color=COL_WOHNUNG, **kwargs)
    return fig

def _barplot(df: pd.DataFrame, x, y, 
             start_date: str=None, end_date: str=None, 
             wohnungen: List[str] = None, 
             **kwargs):
    df_to_plot = select(df, start_date=start_date, end_date=end_date, wohnungen=wohnungen)
    fig = px.bar(df_to_plot, x=x, y=y, **kwargs)
    return default_layout(fig, **kwargs)

def _boxplot(df: pd.DataFrame, x: str, y: str, 
             start_date: str=None, end_date: str=None,
             wohnungen: List[str] = None, 
             **kwargs):
    df_to_plot = select(df, start_date=start_date, end_date=end_date, wohnungen=wohnungen)
    fig=px.box(df_to_plot, x=x, y=y, **kwargs)
    return default_layout(fig, **kwargs)
    
def default_layout(fig, width=2000, height=500, **kwargs):
    fig.update_layout(
        autosize=False,
        width=width,
        height=height
    )
    fig.update_yaxes(automargin=True)
    return fig