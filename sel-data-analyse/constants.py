# Common original Column names
DATETIME_COL = 'Zeitpunkt'

# Common Column names
COL_DATETIME = 'Zeitpunkt'
COL_WOHNUNG = 'Wohnung'
COL_DATE = 'Datum'
COL_HOUR = 'Stunde'
COL_DATE_HOUR = 'Datum-Stunde'

# Energieverbrauch Column names
COL_ZAEHLER_STAND_ENERGIEVERBRAUCH = 'Haushaltsstrom Zaehlerstand [Wh]'
COL_ENERGIEVERBRAUCH = 'Energieverbrauch Haushaltsstrom [Wh]'

# Warmwasserverbrauch Column names
COL_ZAEHLER_STAND_WARMWASSERVERBRAUCH = 'Warmwasserverbrauch Zaehlerstand [m3]'
COL_WARMWASSERVERBRAUCH = 'Warmwasserverbrauch [m3]'


# Heizenergieverbrauch
COL_ZAEHLER_STAND_HEIZENERGIEVERBRAUCH = 'Heizenergie Zaehlerstand [Wh]'
COL_HEIZENERGIEVERBRAUCH = 'Energieverbrauch Heizung [Wh]'

# Wohnung Namen
W1 = "W1"
W2 = "W2"
W3 = "W3"
W4 = "W4"
W5 = "W5"
W6 = "W6"
W7 = "W7"
W8 = "W8"
W9 = "W9"
W_ALLGEMEIN = "Allgemein"

# Helper lists
alle_wohnungen = (W1, W2, W3, W4, W5, W6, W7, W8, W9, W_ALLGEMEIN)
