import pandas as pd
import numpy as np

from typing import List

from constants import *
from load_data import *

__all__ = ["fetch_heizenergieverbrauch_data",
           "fetch_heizenergieverbrauch_alle_wohnungen"]

# Name of the files
HEIZENERGIEVERBRAUCH_DATA_NAME="heizung"

# String of the Wohnung in the filename
FILE_KEY_W1 = "W1"
FILE_KEY_W2 = "W2"
FILE_KEY_W3 = "W3"
FILE_KEY_W4 = "W4"
FILE_KEY_W5 = "W5"
FILE_KEY_W6 = "W6"
FILE_KEY_W7 = "W7"
FILE_KEY_W8 = "W8"
FILE_KEY_W9 = "W9"
FILE_KEY_ALLGEMEIN = "allgemein"
alle_wohnungen_keys = (FILE_KEY_W1, FILE_KEY_W2, FILE_KEY_W3, FILE_KEY_W4, FILE_KEY_W5, FILE_KEY_W6, FILE_KEY_W7, FILE_KEY_W8, FILE_KEY_W9, FILE_KEY_ALLGEMEIN)

_wohnung_by_key = {
    FILE_KEY_W1: W1,
    FILE_KEY_W2: W2,
    FILE_KEY_W3: W3,
    FILE_KEY_W4: W4,
    FILE_KEY_W5: W5,
    FILE_KEY_W6: W6,
    FILE_KEY_W7: W7,
    FILE_KEY_W8: W8,
    FILE_KEY_W9: W9,
    FILE_KEY_ALLGEMEIN: W_ALLGEMEIN
}


# Names of the original (imported) columns
ZAEHLER_STAND_HEIZENERGIEVERBRAUCH_COL = 'Energie [Wh]'

def fetch_heizenergieverbrauch_data(wohnung_key: str)->pd.DataFrame:
    """
    :param wohnung: A substring which appears in the filenames that identifiy the wohnung. example: 'W2'
    """
    # Find correct files
    all_files = fetch_filenames(directory = "./data/heizenergieverbrauch")
    stromverbrauch_files = [fn for fn in all_files if HEIZENERGIEVERBRAUCH_DATA_NAME in fn]
    wohnung_files = [fn for fn in stromverbrauch_files if wohnung_key in fn]
    # read and concatenate dataframes
    dataframes = [read_file(fn) for fn in wohnung_files]
    data = pd.concat(dataframes)
    
    # Convert column names
    data[COL_ZAEHLER_STAND_HEIZENERGIEVERBRAUCH] = data[ZAEHLER_STAND_HEIZENERGIEVERBRAUCH_COL]
    
    # Only keep interesting columns
    data = data[[COL_DATETIME, COL_DATE, COL_HOUR, COL_DATE_HOUR,
                 COL_ZAEHLER_STAND_HEIZENERGIEVERBRAUCH]]
    
    # sort by time
    data.sort_values(by=COL_DATETIME, ignore_index=True, ascending=True, inplace=True)
    
    # add stromverbrauch column (stromverbrauch since the last sample)
    data[COL_HEIZENERGIEVERBRAUCH] = data[COL_ZAEHLER_STAND_HEIZENERGIEVERBRAUCH].diff().fillna(0)
    
    #add wohnung column
    data[COL_WOHNUNG] = get_wohnung_for_key(wohnung_key)
    return data


def fetch_heizenergieverbrauch_alle_wohnungen()->pd.DataFrame:
    """
    Fetches the Energieverbrauch Data for all Wohnungen and merges them into one DataFrame
    """
    dws = {key: fetch_heizenergieverbrauch_data(key) for key in alle_wohnungen_keys}
    merged = pd.DataFrame()
    for key, dw in dws.items():
        merged = merged.append(dw)
    return merged

def get_wohnung_for_key(wohnung_key: str):
    return _wohnung_by_key[wohnung_key]



