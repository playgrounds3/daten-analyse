import pandas as pd
import numpy as np

from constants import *
from load_data import *

from typing import List

def select_date(df: pd.DataFrame, start_date:str=None, end_date:str=None):
    """
    If start_date is None: then takes all rows before (and inclusive) end_date
    If end_date is None: then takes all rows after (and inclusive) start_date
    
    returns a new DataFrame containing only the rows between the start date and end date (both inclusive)
    """
    if start_date is None:
        start_date = df[COL_DATETIME].min()
    if end_date is None:
        end_date = df[COL_DATETIME].max()
    mask = (df[COL_DATETIME] >= start_date) & (df[COL_DATETIME] <= end_date)
    return df.loc[mask]

def select_wohnungen(df: pd.DataFrame, *wohnung_names: List[str]):
    """
    returns a new DataFrame containing only the rows where wohnung is in the keys set
    """
    if not wohnung_names:
        return df
    else:
        mask = (df[COL_WOHNUNG].isin(wohnung_names))
        return df.loc[mask]
    
def select(df: pd.DataFrame, 
           start_date: str=None, end_date: str=None, 
           wohnungen: List[str] = None):
    df_to_return = select_date(df, start_date=start_date, end_date=end_date)
    if wohnungen is not None:
        df_to_return = select_wohnungen(df_to_return, *wohnungen)
    return df_to_return
    

def group_by_hour(df: pd.DataFrame, verbrauch_col: str):
    """
    Groups and sums the samples by hour for each wohnung.
    """
    df_grouped = df.groupby([COL_DATE_HOUR, COL_WOHNUNG])[verbrauch_col].sum().reset_index()
    df_grouped[COL_DATETIME] = df_grouped[COL_DATE_HOUR]
    return df_grouped

def group_by_day(df: pd.DataFrame, verbrauch_col: str):
    """
    Groups and sums the samples by day for each wohnung.
    """
    df_grouped =  df.groupby([COL_DATE, COL_WOHNUNG])[verbrauch_col].sum().reset_index()
    df_grouped[COL_DATETIME] = df_grouped[COL_DATE]
    return df_grouped

