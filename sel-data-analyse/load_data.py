from os import listdir
from os.path import isfile, join
from typing import List
import pandas as pd
import numpy as np

from constants import *

# --- File Methods ---
def fetch_filenames(directory:str)->List[str]:
    """
    Fetches all file names from the given directory
    """
    filenames = [f for f in listdir(directory) if isfile(join(directory, f))]
    return [join(directory, f) for f in filenames]


def read_file(filepath:str)->pd.DataFrame:
    """
    Reads the file into a Data Frame. It is assumed that the file is a csv with ';' as separator
    Also parses the date column.
    """
    # Read File
    data = pd.read_csv(filepath, sep=";")
    
    # Convert to datetime type
    data[COL_DATETIME] = create_col_datetime(data)
    
    # Insert Date and Hour column 
    # TODO: make more efficient and add more the Month
    data[COL_DATE] = pd.DatetimeIndex(data[COL_DATETIME]).date
    data[COL_HOUR] = pd.DatetimeIndex(data[COL_DATETIME]).hour
    data[COL_DATE_HOUR] = pd.to_datetime(data[COL_DATE].astype(str)+' '+data[COL_HOUR].astype(str) + ':00:00')
    data[COL_DATE] = pd.to_datetime(data[COL_DATE].astype(str)+' 00:00')
    
    # convert the columns to correct types
    data[COL_DATETIME] = pd.to_datetime(data[COL_DATETIME])
    data[COL_DATE_HOUR] = pd.to_datetime(data[COL_DATE_HOUR])
    
    return data


def create_col_datetime(df: pd.DataFrame):
    dates_utc = pd.to_datetime(df[COL_DATETIME], infer_datetime_format=True, utc=True)
    return dates_utc.apply(lambda x: pd.to_datetime(x).tz_convert('Europe/Berlin'))